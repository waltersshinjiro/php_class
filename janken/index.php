<?php
require_once("./functions.php");

// submitがpostされていない時（主にtopページ）
if(!isset($_POST["submit"])){
	include("./tmpl/top.tmpl");
	exit;
}

// submitがpostされている時（結果表示ページ）
$error = "";
if(!isset($_POST["janken"])){
	$error = "じゃんけんを選択してください";
} else {
	$hand = intval($_POST["janken"]);

	$computerJanken = 1;
	$result = "";

	if($hand === $computerJanken){
		$result = "あいこ";
	} else if(
		($hand === 0 && $computerJanken === 1) ||
		($hand === 1 && $computerJanken === 2) ||
		($hand === 2 && $computerJanken === 0)
	) {
		$result = "あなたの勝ち";
	} else {
		$result = "あなたの負け";
	}
}

include("./tmpl/result.tmpl");