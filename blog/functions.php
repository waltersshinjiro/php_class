<?php
ini_set('display_errors', "On");

const DB_NAME = "blog";
const DB_ACCOUNT = "root";

function connect_db() {
	$pdo = new PDO("mysql:dbname=".DB_NAME, DB_ACCOUNT);
	return $pdo;
}


function v($data){
	echo "<pre>";
	var_dump($data);
	echo "</pre>";
}

function datetime_format($time) {
	$date = new DateTime($time);
	return $date->format("Y年m月d日 H:i");
}


// バリデーションのためのクラス（機能一式）
class Validation {
	// エラーを格納するためのプロパティを準備する
	private $error = "";

	
	// 入力された値がからかどうかを判定するメソッド
	public function checkEmpty($text, $formName) {
		if(!$text) {
			$this->error .= $formName . "を入力してください。<br>";
		}
	}

	// 文字数が長すぎないかを判定するメソッド
	public function checkMaxLength($text, $formName) {
		if(strlen($text)>100) {
			$this->error .= $formName . "が長すぎます<br>";
		}
	}

	// 文字数が短すぎないかを判定するメソッド
	public function checkMinLength($text, $formName) {
		if($text && strlen($text)<5) {
			$this->error .= $formName . "が短すぎます<br>";
		}
	}

	public function checkAho($text) {
		if(strpos($text,'アホ') !== false || strpos($text,'あほ') !== false) {
			$this->error .= "アホなんて言ってはいけません。<br>";
		}
	}

	// メールアドレスかどうかを判定するメソッド。これは仮です。下記で使用していない&未検証です。
	public function checkmailAddress($text) {
		// 正規表現
		$reg_str = "/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/";
		if(!preg_match($reg_str, $text)) {
			$this->error .= "正しくないメールアドレスです。<br>";
		}
	}

	// エラーを取得するメソッド
	public function get_error() {
		return $this->error;
	}
}