-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- ホスト: localhost
-- 生成日時: 2021 年 5 月 21 日 04:58
-- サーバのバージョン： 10.4.19-MariaDB
-- PHP のバージョン: 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- データベース: `blog`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `content` text NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- テーブルのデータのダンプ `comment`
--

INSERT INTO `comment` (`id`, `post_id`, `name`, `content`, `time`) VALUES
(1, 1, '二宮金次郎', 'ということは明日は水曜日ですね。', '2021-05-18 03:59:12'),
(2, 1, 'ザビエル', 'Tomorrow is Wednesday.', '2021-05-18 03:59:12'),
(3, 2, 'ペリー', 'Momotaro !!!', '2021-05-18 03:59:52'),
(4, 48, '田中一郎', 'ドラえもんですね。', '2021-05-19 15:52:31'),
(5, 71, '湊', 'できた？', '2021-05-20 15:18:19'),
(6, 71, '湊慎二郎', 'でき、、、、た？', '2021-05-20 15:21:19'),
(7, 71, '桃太郎', '鬼退治だ', '2021-05-20 15:26:01'),
(8, 99, 'あああ', 'わわわ', '2021-05-21 01:49:41'),
(9, 71, 'aaaa', 'zzzzz', '2021-05-21 02:28:30'),
(10, 71, 'あああああ', 'わわわわわ', '2021-05-21 02:44:26'),
(11, 71, 'ppppp', 'qqqqqq', '2021-05-21 02:46:50'),
(12, 71, 'ががが', 'ぱぱぱ', '2021-05-21 02:51:38');

-- --------------------------------------------------------

--
-- テーブルの構造 `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `content` text NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- テーブルのデータのダンプ `post`
--

INSERT INTO `post` (`id`, `title`, `content`, `time`) VALUES
(1, '火曜日です。', 'すこし曇っています。', '2021-05-18 01:39:58'),
(2, '明日は水曜日です', '晴れたらいいですね。\r\n\r\n昔々あるところにおじいさんとおばあさんが。\r\n昔々あるところにおじいさんとおばあさんが。\r\n昔々あるところにおじいさんとおばあさんが。\r\n昔々あるところにおじいさんとおばあさんが。\r\n\r\n昔々あるところにおじいさんとおばあさんが。\r\n昔々あるところにおじいさんとおばあさんが。\r\n\r\n\r\n昔々あるところにおじいさんとおばあさんが。昔々あるところにおじいさんとおばあさんが。昔々あるところにおじいさんとおばあさんが。昔々あるところにおじいさんとおばあさんが。昔々あるところにおじいさんとおばあさんが。', '2021-05-18 01:43:14'),
(3, 'テスト', 'テスト', '2021-05-18 06:07:20'),
(4, 'テスト', 'テスト', '2021-05-18 08:41:00'),
(5, 'テスト', 'テスト', '2021-05-18 08:42:43'),
(6, 'aaa', 'bbb', '2021-05-18 08:42:56'),
(7, 'aaa', 'bbb', '2021-05-18 08:43:08'),
(8, 'aaa', 'bbb', '2021-05-18 08:44:50'),
(9, 'aaa', 'bbb', '2021-05-18 08:48:33'),
(10, '題名', 'bbb', '2021-05-18 08:49:41'),
(11, '題名', 'bbb', '2021-05-18 08:56:11'),
(12, '題名', 'bbb', '2021-05-18 08:56:16'),
(13, '題名', 'bbb', '2021-05-18 08:56:43'),
(14, '題名', 'bbb', '2021-05-18 08:56:57'),
(15, '題名', 'bbb', '2021-05-18 08:57:10'),
(16, '題名', 'bbb', '2021-05-18 08:57:17'),
(17, '題名', '本文', '2021-05-18 08:58:31'),
(18, '題名', '本文', '2021-05-18 08:59:23'),
(19, 'できました', 'できたようです。\r\n\r\n\r\nできたようです。', '2021-05-18 09:24:23'),
(20, 'tesafdafa', 'tsaeasefsadfa', '2021-05-18 09:28:22'),
(21, 't', 'te', '2021-05-18 16:18:39'),
(22, 'aaa', 'bbb', '2021-05-19 02:20:06'),
(23, 'xxx', 'yyy', '2021-05-19 02:28:50'),
(24, 'aaa', 'bbb', '2021-05-19 02:32:38'),
(25, 'xxx', 'yyy', '2021-05-19 02:33:47'),
(26, 'xxx', 'yyy', '2021-05-19 02:35:16'),
(27, 'aaa', 'bbb', '2021-05-19 02:49:06'),
(28, 'ppp', 'bbb', '2021-05-19 02:56:11'),
(29, 'xxx', 'bbb', '2021-05-19 02:57:25'),
(30, 'あああ', 'かかか', '2021-05-19 02:58:40'),
(31, 'aaa', 'bbb', '2021-05-19 03:06:19'),
(32, 'xxx', 'yyy', '2021-05-19 05:03:01'),
(33, 'xxx', 'yyy', '2021-05-19 05:05:45'),
(34, 'あああ', 'yyy', '2021-05-19 05:07:10'),
(35, 'あああ', 'わわわ', '2021-05-19 05:07:53'),
(36, 'ppppppp', 'qqqqqqq', '2021-05-19 05:23:38'),
(37, 'ぱぱぱ', 'さささ', '2021-05-19 05:45:00'),
(38, 'tsatsaetesatasadsfasdf', 'tase', '2021-05-19 06:21:44'),
(39, 'dsafasdfasdfasfadsfasdfasd', 'fdsfd', '2021-05-19 06:22:03'),
(40, 'アホ', 'テスト', '2021-05-19 08:04:55'),
(41, 'testtest', 'teest', '2021-05-19 15:30:47'),
(42, 'testtest', 'teest', '2021-05-19 15:31:31'),
(43, 'testtest', 'teest', '2021-05-19 15:33:05'),
(44, 'teststs', 'testesr', '2021-05-19 15:34:01'),
(45, 'teststs', 'testesr', '2021-05-19 15:34:51'),
(46, 'teststs', 'testesr', '2021-05-19 15:35:05'),
(47, 'teststs', 'testesr', '2021-05-19 15:35:24'),
(48, 'tesaseae', 'fadsfa', '2021-05-19 15:51:05'),
(49, 'ああああ', 'わわわわわ', '2021-05-20 02:49:06'),
(50, 'tetse', 'sdafafas', '2021-05-20 05:49:21'),
(51, 'dasffsadfa', 'fsdfa', '2021-05-20 06:14:21'),
(52, 'dasffsadfa', 'fsdfa', '2021-05-20 06:14:29'),
(53, 'dasffsadfa', 'fsdfa', '2021-05-20 06:14:39'),
(54, '', '', '2021-05-20 06:36:48'),
(55, '', '', '2021-05-20 06:40:45'),
(56, '', '', '2021-05-20 06:40:56'),
(57, '新垣結衣と星野源が結婚しました。', '僕らはその頃、プログラミングの勉強を一生懸命していました。', '2021-05-20 06:44:33'),
(71, 'テスト題名', 'テスト本文\r\nテスト本文\r\nテスト本文\r\n\r\nテスト本文', '2021-05-20 07:32:28');

--
-- ダンプしたテーブルのインデックス
--

--
-- テーブルのインデックス `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- ダンプしたテーブルの AUTO_INCREMENT
--

--
-- テーブルの AUTO_INCREMENT `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- テーブルの AUTO_INCREMENT `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
