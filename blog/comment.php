<?php
require_once("functions.php");
$validation = new Validation();

$error = "";
if(isset($_POST["submit"])){
	$post_id = $_POST["post_id"];
	$name = $_POST["name"];
	$content = $_POST["content"];

	$validation->checkEmpty($name, "名前");

	$validation->checkMaxLength($name, "名前");
	$validation->checkEmpty($content, "コメント");

	$error = $validation->get_error();

	if(!$error){
		$pdo = connect_db();
		$st = $pdo->prepare("INSERT INTO `comment` (`post_id`, `name`, `content`) VALUES (?, ?, ?)");
		$st->execute(array($post_id, $name, $content));

		header("Location: index.php");
		exit;
	}
}

if(isset($_GET["post_id"])){
	$post_id = $_GET["post_id"];

	$pdo = connect_db();
	$sql = "SELECT * FROM `post` WHERE `id`=71";
	$st = $pdo->query($sql);
	$posts = $st->fetchAll();
	$post = $posts[0];
} else if(!$error){
	header("Location: index.php");
	exit;
}

// テンプレート読み込み
include("tmpl/header.tmpl");
include("tmpl/comment.tmpl");
include("tmpl/header.tmpl");