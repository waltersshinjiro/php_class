<?php
require_once("functions.php");

$pdo = connect_db();
$st = $pdo->query("SELECT * FROM `post` ORDER BY `id` DESC"); //降順に並び替える
$posts = $st->fetchAll();

// 構造と表現を分離するために、$postの中にそれぞれの記事のコメントを配列で代入する（キーは「"comments"」）
for($i=0; $i<count($posts); $i++){
	$id = $posts[$i]["id"];
	$pdo = connect_db();
	$st = $pdo->query("SELECT * FROM `comment` WHERE `post_id` = $id ORDER BY `id` DESC");//降順に並び替える
	$comments = $st->fetchAll();
	$posts[$i]["comments"] = $comments;
}

// テンプレートを読み込む
include("tmpl/header.tmpl");
include("tmpl/top.tmpl");
include("tmpl/footer.tmpl");