<?php
require_once("functions.php");
$validation = new Validation();

$error = $title = $content = "";
if(isset($_POST["submit"])) {

	$title = $_POST["title"];
	$content = $_POST["content"];

	$validation->checkEmpty($title, "タイトル");
	$validation->checkMaxLength($title, "タイトル");
	$validation->checkMinLength($title, "タイトル");
	$validation->checkAho($title);
	$validation->checkEmpty($content, "本文");
	$validation->checkAho($content);

	$error = $validation->get_error();

	if(!$error) {
		$pdo = connect_db();
		$st = $pdo->prepare("INSERT INTO `post` (`title`, `content`) VALUES (?, ?)");
		$st->execute(array($title, $content));

		if($_FILES["image"]["size"] > 0){
			$id = $pdo->lastInsertId();
			move_uploaded_file($_FILES["image"]["tmp_name"], "imgs/upload_{$id}.jpg");
		}

		header("Location: index.php");
		exit;
	}
}

// テンプレート読み込み
include("tmpl/header.tmpl");
include("tmpl/post.tmpl");
include("tmpl/footer.tmpl");